<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
</head>

<body style="background-color: #f0f0f0; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; margin-top: 10px; margin-bottom: 10px;">

<table style="margin-left: 10%; margin-right:10%; margin-top: 50px; width: 80%;">
    <tr style=" background-color: black;">

        <td style="padding: 5px; border-radius: 5px; color:black; border: 5px solid black;">
            <img style=" width:120px; height:120px; display: block; margin:auto;" src="https://www.experienciasactinvervip.com/assets/img/logo-actinver.svg">
        </td>

    </tr>
</table>


<table style="margin-left: 10%; margin-right:10%; margin-top: 10px; margin-bottom: 10px; width: 80%; padding: 50px; border-radius: 5px; color:black; border: 5px solid black; background-color: black;">
    <tr style=" background-color: black;">

        <td style="color:#ffffff;">
            <h1>@yield('title')</h1>
        </td>
    </tr>
    <tr>
        <td style="color:#ffffff;">
            @yield('content')
        </td>
    </tr>
    <tr>
        <td>
            @yield('call-to-action')
        </td>
    </tr>
</table>


</body>

</html>