<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="manifest.json">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Actinver</title>
    <link href="static/css/main.401a22ab.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <img class="logo" src="assets/img/logo-actinver.svg"/>
    <nav>
        <span id="scrollTop">Inicio</span>
        <span id="scrollBenefits">Beneficios</span>
        <a href="{{ route('certificate') }}" id="login">
            CERTIFICADO
        </a>
        <a href="{{ route('login') }}" id="login">LOGIN</a>
    </nav>
</header>
<main>
    <img src="assets/img/6th-avenue.jpg"/>

    <a href="tel:015547449338"><img class="centered-concierge" src="assets/img/concierge.png"></a>

    <!--<img id="header-f1" src="assets/img/header-f1.png">

    <div id="f1" class="f1">

        <img class="centered" src="assets/img/1.png">
        <hr>

        <img class="centered" src="assets/img/2.png">
        <hr>

        <img class="centered" src="assets/img/3.png">
        <hr>

        <img class="centered" src="assets/img/4.png">
        <hr>

        <img class="centered" src="assets/img/5.png">


    </div>
    
    -->

    <hr class="final-hr">


    <div id="benefits" class="panels">
        <div class="info">
            <h3>EN ACTINVER NOS INTERESA HACER SENTIR A NUESTROS CLIENTES COMO LO MÁS IMPORTANTE.</h3>
 
 
<p>Te presentamos Actinver VIP, un programa de lealtad para nuestros clientes exclusivos, que les permitirá disfrutar de privilegios únicos con la Tarjeta Experiencias Actinver VIP que les da accesos a:</p>
<ul>
<li>Concierge personalizado</li>
<li>Organización de viajes a la medida</li>
<li>Tarifa preferencial y beneficios en 400,000 hoteles</li>
<li>Oportunidad de ascensos a precios preferenciales</li>
<li>Beneficios y paquetes exclusivos en los mejores eventos</li>
<li>Eventos deportivos nacionales e internacionales</li>
<li>Selección especial de vinos</li>
<li>Promociones especiales en restaurantes seleccionados</li>
<li>Precios preferenciales en productos y marcas de moda</li>
<li>Belleza y joyería</li>
<li>Paquetes y descuentos especiales en spas seleccionados</li>
<li>Accesos a eventos exclusivos</li>
</ul>
 
        </div>
        <div class="images">
            <div class="half" style="background-image:url(assets/img/islands.jpg)"></div>
            <div class="half" style="background-image:url(assets/img/hotel.jpg)"></div>
            <div style="background-image:url(assets/img/beach.jpg)"></div>
        </div>
        <div class="icon">
            <h3>Beneficios</h3>
            <img src="assets/img/airplane.png"/>
            <div>
                <h4>Ascensos con Aeromexico</h4>
                <h5>(Ascensos en vuelos a categoría premier)</h5>
            </div>
        </div>
        <div class="big-image">
            <h3>Plataforma de Viajes</h3>
            <img src="assets/img/pier.jpg"/>
            <p>La plataforma y los servicios de soporte son 100% personalizados</p>
        </div>
        <div class="icon inverted"><img src="assets/img/microphone.png"/> <span>Eventos exclusivos a nivel mundial</span></div>
        <div class="big-image">
            <img src="assets/img/hyundai.jpg"/> <strong>Alquiler de Autos</strong>
            <p>Puedes elegir con más de 30 proveedores de alquiler de autos</p>
        </div>
        <div class="icon"><img src="assets/img/football.png"/> <span>Eventos deportivos a nivel mundial</span></div>
        <div class="big-image">
            <img src="assets/img/sport.jpg"/> <strong>Actividades</strong>
            <p>20,000 actividades disponibles en más de 1,000 ciudades alrededor del mundo</p>
        </div>
    </div>
</main>
<footer id="certificate">
</footer>
<script type="text/javascript" src="static/js/main.d31a4de3.js"></script>
</body>
</html>