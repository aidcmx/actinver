<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="manifest.json">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Actinver</title>
    <link href="static/css/main.401a22ab.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">

</head>
<body>
<header>
    <img class="logo" src="assets/img/logo-actinver.svg"/>
</header>
<main>
    <form style="margin-top: 40px" action="{{ route('resetPassword') }}" method="post">

        <h1>Recuperar contraseña: </h1>

        <p>Estas por reestablecer la contraseña para: {{ $user->email }}</p>

        <fieldset>


            <label for="password">Password:</label>
            <input type="password" id="password" name="user_password">

            <label for="password">Confirmar password:</label>
            <input type="password" id="confirm" name="user_password_confirm">

            <input type="hidden"  name="token" value="{{$token}}">

        </fieldset>

        <button type="submit">Enviar</button>

    </form>
    <br><br>
</main>

<script type="text/javascript" src="static/js/main.d31a4de3.js"></script>
</body>
</html>