@extends('layouts.mail')

@section('content')

    <center>
        <p>
            Este correo ha sido enviado porque pediste que te ayudaramos a reestablecer tu contraseña. Para hacerlo, da click en el link en la parte inferior.
        </p>
        <p>
            Si no fuiste tu quién pidió reestablecer tu contraseña, puedes ignorar este correo.
        </p>
    </center> <br><br>

@endsection

@section('title')
    <center>Establecer contraseña</center>
@endsection

@section('call-to-action')
    <center><a href="https://www.experienciasactinvervip.com/recover?token={{$token->token}}"> <button style="font-size: 20px; border-radius: 25px; border: 2px solid #b5a16b; padding: 10px 30px 10px 30px; background-color: #b5a16b; color:white;">Establecer contraseña</button> </a></center>
@endsection