<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="manifest.json">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Actinver</title>
    <link href="static/css/main.401a22ab.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">

</head>
<body>
<header>
    <img class="logo" src="assets/img/logo-actinver.svg"/>
    <nav>
        <a href="{{ route('register') }}" id="login">REGISTRO</a>
        <a href="{{ route('login') }}" class="header-a">LOGIN</a>
    </nav>
</header>
<main>
    <form style="margin-top: 40px" action="{{ route('emailSent') }}" method="post">

        <p>
            <h1>Reestablecer contraseña: </h1>
        </p>

        <p>Escribe el correo con el que estás registrado y te enviaremos un link para reestablecer tu contraseña.</p>

        <fieldset>

            <label for="mail">Correo Electrónico:</label>
            <input type="email" id="mail" name="user_email">

        </fieldset>

        <button type="submit">Enviar</button>

    </form>
    <br><br>
</main>
<footer id="certificate">

</footer>
<script type="text/javascript" src="static/js/main.d31a4de3.js"></script>
</body>
</html>