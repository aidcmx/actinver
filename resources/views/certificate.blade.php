<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="manifest" href="manifest.json">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Actinver</title>
    <link href="static/css/main.401a22ab.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">

</head>
<body>
<header>
    <img class="logo" src="assets/img/logo-actinver.svg"/>
    <nav>
        <a href="#" class="header-a">REGISTRO</a>
        <a href="{{ route('login') }}" class="header-a">LOGIN</a>
    </nav>
</header>
<main>

</main>
<footer id="certificate">
    <h3>Certificado de Regalo</h3>
    <img src="assets/img/certificate.jpg"/> <a href="{{ route('register') }}" class="button">Activar Certificado</a>
</footer>
<script type="text/javascript" src="static/js/main.d31a4de3.js"></script>
</body>
</html>