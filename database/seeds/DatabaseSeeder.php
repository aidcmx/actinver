<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new User;
        $user->code = "123456VIP";
        $user->save();

        $user = new User;
        $user->code = "234567VIP";
        $user->save();

        $user = new User;
        $user->code = "345678VIP";
        $user->save();

        $user = new User;
        $user->code = "456789VIP";
        $user->save();

        $user = new User;
        $user->code = "567890VIP";
        $user->save();

        $user = new User;
        $user->code = "678901VIP";
        $user->save();

        $user = new User;
        $user->code = "789012VIP";
        $user->save();

        $user = new User;
        $user->code = "890123VIP";
        $user->save();

        $user = new User;
        $user->code = "901234VIP";
        $user->save();

        $user = new User;
        $user->code = "012345VIP";
        $user->save();

        $user = new User;
        $user->code = "098765VIP";
        $user->save();

        $user = new User;
        $user->code = "987654VIP";
        $user->save();

        $user = new User;
        $user->code = "876543VIP";
        $user->save();

        $user = new User;
        $user->code = "876143VIP";
        $user->save();


    }
}
