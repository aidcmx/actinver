<?php

namespace App\Http\Controllers;

use App\Mail\Recover;
use App\User;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function home()
    {
        return view('home');
    }

    public function certificate()
    {
        return view('certificate');
    }

    public function register()
    {
        return view('register');
    }

    public function login()
    {

        $shouldShowReset = false;

        return view('login',compact('shouldShowReset'));

    }

    public function validateLogin(Request $request)
    {

        $email = $request->input("user_email");
        $password = $request->input("user_password");

        $user = User::where('email', '=', $email)->first();

        if (!$user) {
            //TODO DISPLAY DE ESTE ERROR
            return "El usuario no existe";
        }

        if (Hash::check($password, $user->password)) {

            $result = $this->makeApiRequest("https://api.saveonresorts.com/clubmembership/getlogintokennovalidation", ["Email" => $email]);

            $response = json_decode($result->getBody());
            $token = str_replace("LoginToken:", "", $response);

//            return redirect("https://viaje.experienciasactinvervip.com/vacationclub/logincheck.aspx?Token=$token");
            return redirect("https://bookings.6thavenue.mx/vacationclub/logincheck.aspx?Token=$token");

        } else {
            return "false";
        }

    }

    public function submit(Request $request)
    {

        $email = $request->input("user_email");
        $name = $request->input("user_name");
        $lastname = $request->input("user_lastname");
        $password = $request->input("user_password");
        $repeat = $request->input("user_password_confirm");
        $code = strtolower($request->input("user_code"));

        $user = User::where('code', '=', $code)->first();

        if ($user == null) {
            //TODO DISPLAY DE ESTE ERROR
            return ("Este codigo no existe");
        }

        if ($user->status == "active") {
            //TODO DISPLAY DE ESTE ERROR
            return ("Este codigo ya ha sido usado");
        }

        $array = [
            "Email" => $email,
            "Password" => $password,
            "FirstName" => $name,
            "LastName" => $lastname,
            "UserAccountTypeID" => 5,
        ];

        $result = $this->makeApiRequest("https://api.saveonresorts.com/v2/clubmembership/createdefault", $array);

        $response = json_decode($result->getBody());


        if ($response->ResultType == "error") {
            return ($response->Message);
        } else {

            $newUser = User::where('code', '=', $code)->first();
            $newUser->platform_id = $response->Account->UserId;
            $newUser->name = $name;
            $newUser->lastName = $lastname;
            $newUser->email = $email;
            $newUser->code = $code;
            $newUser->status = "active";
            $newUser->password = app('hash')->make($password);
            $newUser->save();

            $result = $this->makeApiRequest("https://api.saveonresorts.com/clubmembership/getlogintokennovalidation", ["Email" => $email]);

            $response = json_decode($result->getBody());
            $token = str_replace("LoginToken:", "", $response);

//            return redirect("https://viaje.experienciasactinvervip.com/vacationclub/logincheck.aspx?Token=$token");
            return redirect("https://bookings.6thavenue.mx/vacationclub/logincheck.aspx?Token=$token");

        }
    }

    public function forgot()
    {
        return view('forgot');
    }

    public function forgotSent(Request $request)
    {

        $mail = $request->input("user_email");

        $user = User::where('email','=',$mail)->first();


        if($user != NULL){


            $token = new Token;
            $token->user_id = $user->id;
            $token->token = md5(time());
            $token->save();

            Mail::to($user->email)->send(new Recover($token));

            return view('forgot-confirmation');

        }else{

            //TODO DISPLAY DE ESTE ERROR
            return view('forgot');
        }

    }

    public function recover(){

        $token = $_GET["token"];

        if($token != null){

            $instance = Token::where('token','=',$token)->first();

            if($instance != null){

                $user = $instance->user;

                return view('recover',compact('user','token'));

            }else{
                //TODO DISPLAY DE ESTE ERROR

            }

        }else{
            //TODO DISPLAY DE ESTE ERROR

        }

    }

    public function resetPassword(Request $request){

        $token = $request->input('token');
        $password = $request->input('user_password');

        $instance = Token::where('token','=',$token)->first();
        $user = $instance->user;

        if(!$instance->valid){
            //TODO DISPLAY DE ESTE ERROR

        }

        $user->password = app('hash')->make($password);
        $user->save();

        $instance->valid = false;
        $instance->save();

        $shouldShowReset = true;

        //TODO ACTUALIZAR EN LA PLATAFORMA


        return view('login',compact('shouldShowReset'));

    }

}
