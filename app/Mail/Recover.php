<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class Recover extends Mailable
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('forgot-mail')
            ->subject('Reestablecer contraseña')
            ->with([
                'token' => $this->token,
            ]);

    }

    public function __construct($token)
    {
        $this->token = $token;

    }
}