<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/',[
        'as' => 'home',
        'uses' => 'ExampleController@login']
);


$router->get('register',[
        'as' => 'register',
        'uses' => 'ExampleController@register']
);


$router->post('submit',[
        'as' => 'submit',
        'uses' => 'ExampleController@submit']
);

$router->get('login',[
        'as' => 'login',
        'uses' => 'ExampleController@login']
);

$router->get('certificate',[
        'as' => 'certificate',
        'uses' => 'ExampleController@certificate']
);

$router->post('validateLogin',[
        'as' => 'validateLogin',
        'uses' => 'ExampleController@validateLogin']
);

$router->get('forgot',[
        'as' => 'forgot',
        'uses' => 'ExampleController@forgot']
);

$router->post('forgot',[
        'as' => 'emailSent',
        'uses' => 'ExampleController@forgotSent']
);


$router->get('recover',[
        'as' => 'recover',
        'uses' => 'ExampleController@recover']
);

$router->post('recover',[
        'as' => 'resetPassword',
        'uses' => 'ExampleController@resetPassword']
);


